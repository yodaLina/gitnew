/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package annotations;

import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author ITU
 */
public class DataCache {
    private List<BaseModele>listeEntities;
    private Timestamp datefin;

    public Timestamp getDatefin() {
        return datefin;
    }

    public void setDatefin(Timestamp datefin) {
        this.datefin = datefin;
    }
    public List<BaseModele> getListeEntities() {
        return listeEntities;
    }

    public void setListeEntities(List<BaseModele> listeEntities) {
        this.listeEntities = listeEntities;
    }

    
}
