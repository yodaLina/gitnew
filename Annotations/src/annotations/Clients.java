/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package annotations;

/**
 *
 * @author ITU
 */
@Table("clients")
@Sequential(sequenceName="clients_S",sequenceStrat=true,sequenceSuffix="CL0")
@Cache(valeur=true,time=100000,utilisationType="IUF")
public class Clients extends BaseModele{
    @Colonne("nom")
    private String nom;
    @Colonne("devise")
    private String devise;
    private String danger;
    public String getDanger() {
        return danger;
    }

    public void setDanger(String danger) {
        this.danger = danger;
    }
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    public Clients(String id) {
        super(id);
    }
}
