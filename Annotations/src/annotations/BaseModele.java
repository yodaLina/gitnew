/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package annotations;

/**
 *
 * @author Andi
 */
public abstract class  BaseModele {
    @Id
    @Colonne("id")        
    private String id;
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BaseModele(String id) {
        this.id = id;
    }
    
}
