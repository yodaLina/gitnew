/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package annotations;

import java.sql.Connection;
import java.util.ArrayList;

/**
 *
 * @author Andi
 */
public interface InterfaceDAO {
    public void save(Connection con,BaseModele m) throws Exception;
    public void update(Connection con,BaseModele m) throws Exception;
    public void delete(Connection con,BaseModele m) throws Exception;
    public BaseModele findById(Connection con,BaseModele m) throws Exception;
    public ArrayList<BaseModele> findAll(Connection con,BaseModele m) throws Exception;
    public ArrayList<BaseModele> findAll(Connection con,BaseModele m,int max,int offset) throws Exception;
    
}
