/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package annotations;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author ASUS
 */
public class Connexion {
    public static Connection getConnexion(){
        Connection con = null;
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","Bank","lol");
            con.setAutoCommit(false);
            if(con!=null){
                System.out.println("Connecté");
            }
        }
        catch(ClassNotFoundException | SQLException e){
            e.printStackTrace();
        }
        return con;
    }
    
}
