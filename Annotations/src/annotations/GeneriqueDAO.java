/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package annotations;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Andi
 */
public class GeneriqueDAO implements InterfaceDAO{
    private HashMap<Class,DataCache>listecache=new HashMap();
    public static String toUpperfirst(String value){
        return String.valueOf(value.charAt(0)).toUpperCase()+value.substring(1);
    }
    public Object FetchValues(ResultSet rs, BaseModele m) throws Exception{
        Class cm=m.getClass();
        Field[] fields = cm.getDeclaredFields();
        Class[]liste=null;
        Class csm=cm.getSuperclass();
        Class id=csm.getDeclaredField("id").getType();
        Constructor cons=cm.getConstructor(id);
        Object nouvelleinstance=cons.newInstance(m.getId());
        for(Field f : fields){
            try{
                Annotation a=cm.getAnnotation(Colonne.class);
                if(a!=null){
                    liste=new Class[1];
                    liste[0]=f.getType();
                    Object o=rs.getObject(f.getAnnotation(Colonne.class).value());
                    Method meth=cm.getMethod("set"+GeneriqueDAO.toUpperfirst(f.getName()),liste);
                    meth.invoke(nouvelleinstance,liste[0].cast(o));
                }
            }
            catch(Exception e){
                e.printStackTrace();
                throw e;
            }
        }
        return nouvelleinstance;
    }
    @Override
    public void save(Connection con, BaseModele m) throws Exception{
        PreparedStatement ps=null;
        try{
            ps=GeneriqueDAO.genererStatement(m, con);
            ps.executeUpdate();
        }
        catch(Exception e){
            if(con!=null){
                con.rollback();
            }
            e.printStackTrace();
            throw e;
        }
        finally{
            if(ps!=null){
                ps.close();
            }
        }
    }

    @Override
    public void update(Connection con, BaseModele m) throws Exception{
        PreparedStatement ps =null;
        try{
            ps=GeneriqueDAO.formerReqUpdate(m, con);
            ps.executeQuery();
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally{
            if(ps!=null){
                ps.close();
            }
        
        }
    }

    @Override
    public void delete(Connection con, BaseModele m) throws Exception {
        PreparedStatement s=null;
         Class cl = con.getClass();
        Table table = (Table)cl.getAnnotation(Table.class);
        String tableName = table.value();
        String st = "delete "+tableName+" where id=?";
	try
        {	
           s = con.prepareStatement(st);
           s.setObject(1,m.getId());            
           s.executeUpdate();
           con.commit();
        }
        catch(Exception ex)
        {
           ex.printStackTrace();
            throw ex;
        }
        finally
        {
            if(s!=null)
            {
                s.close();
            }     
        } 
    }
    @Override
    public BaseModele findById(Connection con, BaseModele m) throws Exception {
        BaseModele b = null;
        Class c = m.getClass();
        Table table = (Table)c.getAnnotation(Table.class);
        String tableName = table.value();
        PreparedStatement pStat = null;
        ResultSet rs = null;
        try{
            String query ="select * from "+tableName+" where id = ?";
            pStat = con.prepareStatement(query);
            pStat.setObject(1,m.getId());
            rs = pStat.executeQuery();
            if(rs.next()){
                b =(BaseModele)FetchValues(rs,m);
            }
            return b;
        }
        catch(Exception e){
            e.printStackTrace();
            throw e;
        }
        finally{
            if(rs!=null){
               rs.close();
           }
           if(pStat!=null){
               pStat.close();
           }
        }
    }
    public void ControlCacheLister(BaseModele bm,ArrayList<BaseModele>liste){
        Cache c=bm.getClass().getAnnotation(Cache.class);
        if(c.valeur()){
            if(c.utilisationType().contains("L")){
                if(listecache.get(liste.getClass())==null){
                    DataCache d=new DataCache();
                    d.setDatefin(new Timestamp(System.currentTimeMillis()+c.time()*100));
                    listecache.put(bm.getClass(),d);
                }
                if(listecache.get(liste.getClass()).getListeEntities()==null){
                    listecache.get(bm.getClass()).setListeEntities(liste);
                }
            }
        }
    }
    public static PreparedStatement genererStatement(BaseModele m,Connection c) throws Exception{
        String listecols="";
        String listevals="";
        Field[]listef=m.getClass().getDeclaredFields();
        Class cl=m.getClass();
        Sequential s=(Sequential) cl.getAnnotation(Sequential.class);
        if(s!=null){
            if(s.sequenceStrat()){
                listevals+="concat('"+s.sequenceSuffix()+"',"+s.sequenceName()+".nextval),";
                listecols+="id,";
            }
            else{
                listevals+=s.sequenceName()+".nextval,";
                listecols+="id,";
            }
        }
        Table tab=(Table) cl.getAnnotation(Table.class);
        ArrayList ar=new ArrayList();
        PreparedStatement ps=null;
        Class[]listen=null;
        try{
            for(Field f:listef){
                Annotation an=f.getAnnotation(Colonne.class);
                if(an!=null){
                   Colonne col=(Colonne)an;
                   Method meth=cl.getMethod("get"+GeneriqueDAO.toUpperfirst(f.getName()),listen);
                   Object resp=meth.invoke(m);
                   if(resp!=null){
                       listecols+=col.value()+",";
                       listevals+="?,";
                       ar.add(resp);
                   }
                }
            }
            listecols=listecols.substring(0,listecols.length()-1);
            listevals=listevals.substring(0,listevals.length()-1);
             ps=c.prepareStatement("Insert into "+tab.value()+"("+listecols+") values("+listevals+")");
            int taille=ar.size();
            for(int i=1;i<=taille;i++){
                ps.setObject(i,ar.get(i-1));
            }
           return ps;
        }
        catch(Exception e){
            e.printStackTrace();
            throw e;
        }
    }
    @Override
    public ArrayList<BaseModele> findAll(Connection con, BaseModele m) throws Exception {
        ArrayList<BaseModele> liste = new ArrayList();
        Class c = m.getClass();
        Table table = (Table)c.getAnnotation(Table.class);
        String tableName = table.value();
        PreparedStatement pStat = null;
        ResultSet rs = null;
        try{
            String query ="select * from "+tableName;
            pStat = con.prepareStatement(query);
            rs = pStat.executeQuery();
            while(rs.next()){
                liste.add((BaseModele)FetchValues(rs,m));
            }
            return liste;
        }
        catch(Exception e){
            e.printStackTrace();
            throw e;
        }
        finally{
            if(rs!=null){
               rs.close();
           }
           if(pStat!=null){
               pStat.close();
           }
        }
    }
    public static PreparedStatement formerReqUpdate(BaseModele e,Connection c) throws Exception{
      String rep=null;
      PreparedStatement ps=null;
      ArrayList ar=new ArrayList();
      try{
          rep="";
            Field[] colonne = e.getClass().getDeclaredFields();
            int tailleC = colonne.length;
            for(int index=0;index<tailleC;index++){
                Colonne cx= colonne[index].getAnnotation(Colonne.class);
                if(cx!=null)
                {
                    String maj =""+colonne[index].getName().charAt(0);
                    String colonne1 = maj.toUpperCase()+colonne[index].getName().substring(1);
                    Method get =e.getClass().getMethod("get"+colonne1,null);
                    Object resp=get.invoke(e,null);
                    if(index==tailleC-1){ 
                        if(resp!=null){
                               rep+=cx.value()+"=?";
                               ar.add(resp);
                        }
                    }
                    else{
                        if(resp!=null){
                               rep+=cx.value()+"=?";
                               ar.add(resp);
                               rep+=",";
                        }
                    }
                }
                else{
                    if(index==tailleC-1){
                        rep=rep.substring(0,rep.length()-1);
                    }
                }
            }
            Table t=(Table)e.getClass().getAnnotation(Table.class);
            ps=c.prepareStatement("Update "+t.value()+" set "+rep+" where id="+e.getId());
            int tx=ar.size();
            for(int i=1;i<=tx;i++){
                ps.setObject(i,ar.get(i-1));
            }
            return ps;
            
      }
      catch(Exception h){
          h.printStackTrace();
          throw h;
      }
    }
    @Override
    public ArrayList<BaseModele> findAll(Connection con, BaseModele m, int max, int offset) throws Exception{
        ArrayList<BaseModele> liste = new ArrayList();
        Class c = m.getClass();
        Table table = (Table)c.getAnnotation(Table.class);
        String tableName = table.value();
        PreparedStatement pStat = null;
        ResultSet rs = null;
        try{
            String query ="select * from "+tableName+" limit ? offset ?";
            pStat = con.prepareStatement(query);
            pStat.setInt(1,max);
            pStat.setInt(2,offset);
            rs = pStat.executeQuery();
            while(rs.next()){
                liste.add((BaseModele)FetchValues(rs,m));
            }
            return liste;
        }
        catch(Exception e){
            e.printStackTrace();
            throw e;
        }
        finally{
            if(rs!=null){
               rs.close();
           }
           if(pStat!=null){
               pStat.close();
           }
        }
    }
}
