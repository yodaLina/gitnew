/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import DAO.ClientDAO;
import Model.Client;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.portlet.ModelAndView;

/**
 *
 * @author FATF
 */
@Controller
public class Controlleur {
    private ClientDAO cl;

    public ClientDAO getCl() {
        return cl;
    }
    @Autowired(required=true)
    @Qualifier(value="clDAO")
    public void setCl(ClientDAO cl) {
        this.cl = cl;
    }
    @RequestMapping(value = "/persons", method = RequestMethod.GET)
    public String listPersons(Model d) {
        List<Client>liste=cl.lister();
        d.addAttribute("Liste",liste);
        System.out.println(liste.size());
        return "index";
    }
}
