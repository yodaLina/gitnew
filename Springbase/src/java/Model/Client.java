/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author FATF
 */
@Entity
@Table(name="Clients")
public class Client {
    @Id
    @Column(name="ID")
    private String id;
    @Column(name="nom")
    private String nom;
    @Column(name="devise")
    private String devise;

    public Client(String id, String nom, String devise) {
        this.id = id;
        this.nom = nom;
        this.devise = devise;
    }
    public Client(){}
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }
}
