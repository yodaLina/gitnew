/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.Client;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author FATF
 */
public class ClientDAOImpl implements ClientDAO{
    private SessionFactory sf;

    public SessionFactory getSf() {
        return sf;
    }

    public void setSf(SessionFactory sf) {
        this.sf = sf;
    }
    
    @Override
    public List<Client> lister() {
        Session s=sf.openSession();
        List<Client>liste=s.createQuery("from Client").list();
        s.close();
        return liste;      
    }


    
}
