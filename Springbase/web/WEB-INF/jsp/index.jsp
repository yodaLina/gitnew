<%@page import="Model.Client"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns:th="http://www.thymeleaf.org">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Welcome to Spring Web MVC project</title>
    </head>

    <body>
        <p>Hello! This is the default welcome page for a Spring Web MVC project.</p>
        <p><i>To display a different welcome page for this project, modify</i>
            <tt>index.jsp</tt> <i>, or create your own welcome page then change
                the redirection in</i> <tt>redirect.jsp</tt> <i>to point to the new
                welcome page and also update the welcome-file setting in</i>
            <tt>web.xml</tt>.</p>
    </body>
    <p>${Liste.size()}</p>
    <table border="1" width="800">
        <thead>
            <tr>
                <td>Nom</td>
                <td>Devise</td>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="lone" items="${Liste}" >
                <tr>
                    <td>adasd</td>
                    <td>${lone.getNom()}</td>
                    <td>${lone.getDevise()}</td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</html>
